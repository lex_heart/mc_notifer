from unittest.mock import MagicMock, patch

import pytest

import minecraft_server.configuration_parser as parser


def args_to_arg_array(args_dict):
    arg_array = ['mcbot', 'configure']
    for k, v in args_dict.items():
        arg_array.append('--{}'.format(k))
        arg_array.append(str(v))
    return arg_array


def all_valid_args_base():
    return {
        "addr": "mock-server.gg",
        "port": 1234,
        "channel": "mock channel",
    }


def all_valid_args_array():
    return args_to_arg_array(all_valid_args_base())


def all_valid_args_but_one(missing_arg):
    args = all_valid_args_base()
    del args[missing_arg]
    return args_to_arg_array(args)


def test_configuration_parser_trows_when_no_args():
    with pytest.raises(ValueError):
        args = []
        parser.parse(args)


def test_parser_throws_when_missing_all_configuration_args():
    with pytest.raises(ValueError):
        args = ['mcbot', 'configure']
        parser.parse(args)


def test_parser_throws_when_missing_addr():
    with pytest.raises(ValueError, match='missing: addr'):
        args = all_valid_args_but_one('addr')
        parser.parse(args)


def test_parser_throws_when_missing_port():
    with pytest.raises(ValueError, match='missing: port'):
        args = all_valid_args_but_one('port')
        parser.parse(args)


def test_parser_throws_when_missing_channel():
    with pytest.raises(ValueError, match='missing: channel'):
        args = all_valid_args_but_one('channel')
        parser.parse(args)


def test_parser_with_all_values():
    expected = all_valid_args_base()

    args = all_valid_args_array()
    actual = parser.parse(args)

    assert actual is not None
    for k in expected:
        assert expected[k] == actual.__dict__[k]


def test_help_string_calls_fixup_and_returns_result():
    expected = 'oops we modified a lot'
    fixup = MagicMock(return_value=expected)
    actual = parser.help_string(fixup)
    fixup.assert_called_once()
    assert expected == actual


def test_help_string_captures_and_returns_argparse_help_output():
    expected = "this is a fake help message"

    fake_print = lambda: print(expected, end='')
    with patch('argparse.ArgumentParser.print_help', side_effect=fake_print):
        noop = lambda x: x
        actual = parser.help_string(noop)

    assert actual == expected


