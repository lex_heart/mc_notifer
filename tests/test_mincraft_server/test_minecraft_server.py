from minecraft_server import server

def test_basic_construction():
    expected_addr = "test-addr.gg"
    expected_port = 1235
    expected_channel = "mock channel"
    expected_guild = "mock guild"

    actual_server = server.MinecraftServer(expected_addr, expected_port, expected_channel, expected_guild)

    assert expected_addr == actual_server.addr
    assert expected_port == actual_server.port
    assert expected_channel == actual_server.channel
    assert expected_guild == actual_server.guild
