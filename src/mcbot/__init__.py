from discord.ext import commands
import mcbot.bot_fun
import mcbot.bot_core
from global_state import configuration


def launch(token):
    bot = commands.Bot(command_prefix='mcbot ')
    mcbot.bot_fun.add_commands(bot)
    mcbot.bot_core.add_commands(bot)
    configuration.bot = bot
    bot.run(token)
