from functools import wraps

from discord.ext.commands import Context

import global_state


def global_admin(func):
    @wraps(func)
    async def __wrapper__(ctx: Context, *args, **kwargs):
        if ctx.author.id == global_state.get_global_administrator():
            return await func(ctx, *args, **kwargs)
    return __wrapper__

