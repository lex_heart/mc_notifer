from discord.ext.commands import Context

from global_state import configuration


async def boop(ctx: Context):
    auth_id_str = str(ctx.author.id)
    response = configuration.get('boop', auth_id_str, fallback='boop')
    await ctx.send(response)


def add_commands(bot):
    bot.command()(boop)
