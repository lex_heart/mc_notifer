import os
from datetime import datetime
from sys import argv

import discord
from discord.ext.commands import Context

import minecraft_server.configuration_parser as mc_cfg
import minecraft_server.server as mc_server
import global_state
from mcbot.bot_helpers import global_admin


async def configure(ctx: Context):
    '''
    Parses the configuration command and adds a server
    Mapping: Server<->discord channel on server
    '''

    def fixup(x):
        filename = os.path.split(argv[0])[1]
        x = x.replace(filename, "mcbot configure")
        return x.replace('and exit', '')

    if ' -h' in ctx.message.content or ' --help' in ctx.message.content:
        help_str = mc_cfg.help_string(fixup)
        await ctx.send(f"```\n{help_str}\n```")
    else:
        args = ctx.message.content.split(' ')
        try:
            config = mc_cfg.parse(args)
            server = mc_server.MinecraftServer(addr=config.addr, port=config.port, channel=ctx.channel, guild=ctx.guild)
            await ctx.send(f"Configured {config.addr}:{config.port}")
            server.launch()
        except ValueError as e:
            help_str = mc_cfg.help_string(fixup)
            await ctx.send(f"failed to configure: **{e}**\n\n```\n{help_str}\n```")


@global_admin
async def diagnostics(ctx: Context):
    '''
    Output diagnostics to channel
    :param ctx: context that command was given
    '''
    td = datetime.utcnow() - global_state.start
    embed = discord.Embed(title="Diagnostics", color=0xF4AF23)
    embed.set_thumbnail(
        url="https://cdn.discordapp.com/attachments/746923357504536597/753152736010174494/hiclipart.com.png")
    runtime_stats = [f"**Started**: {str(global_state.start)}",
                     f"**Uptime**: {str(td)}"]
    embed.add_field(name="runtime info", value='\n'.join(runtime_stats))
    embed.set_footer(text="Bot by Lex<3")
    await ctx.send(embed=embed)


@global_admin
async def fake_disconnect(ctx: Context):
    server = mc_server.get_server(ctx.guild.id)
    if server:
        await server.send_disconnect()


@global_admin
async def fake_reconnect(ctx: Context):
    server = mc_server.get_server(ctx.guild.id)
    if server:
        await server.send_reconnect()


def add_commands(bot):
    bot.command()(configure)
    bot.command()(diagnostics)
    bot.command()(fake_disconnect)
    bot.command()(fake_reconnect)
