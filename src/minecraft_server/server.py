import asyncio
from contextlib import suppress
from enum import Enum
from typing import Dict
from typing import List, Optional

import discord
import mcstatus
import mcstatus.pinger

servers = {}  # type: Dict[int, MinecraftServer]


# TODO add a name field
# TODO configure server image
class MinecraftServer:
    class States(Enum):
        CONNECTED = 1
        FAILING = 2
        DISCONNECTING = 3
        DISCONNECTED = 4

    # TODO port support
    def __init__(self, addr, port, channel, guild):
        self.state = MinecraftServer.States.CONNECTED
        self.failures = 0
        self.addr = addr
        self.port = port
        self.guild = guild
        self.channel = channel
        self.mc_server = mcstatus.MinecraftServer.lookup(f"{addr}")
        self.players = set()
        # TODO allow channel+guild identification
        if guild.id in servers:
            servers[guild.id].deregister()
        servers[guild.id] = self
        self.task = None

    # region utility
    def launch(self):
        if not self.task:
            self.task = asyncio.Task(self.periodic_update())

    def deregister(self):
        if self.task:
            self.task.cancel()

    # endregion

    # region minecraft communication
    def update_players(self) -> (List[str], List[str]):
        status = self.mc_server.status()  # type: mcstatus.pinger.PingResponse
        cur_players = {x.name for x in status.players.sample} if status.players.sample else set()
        logged_off = self.players - cur_players
        online = self.players.intersection(cur_players)
        logged_on = cur_players - self.players
        self.players = cur_players
        return logged_off, online, logged_on

    def ping(self):
        return self.mc_server.ping()

    # endregion

    # region discord communication
    async def send_status(self, logged_off, online, logged_on):
        status_list = []
        status_list.extend((x, ":new:") for x in logged_on)
        status_list.extend((x, ":green_circle:") for x in online)
        status_list.extend((x, ":red_circle:") for x in logged_off)

        if logged_on or logged_off:
            # TODO use name in title
            embed = discord.Embed(title=self.addr, color=0xa1e2c1)
            embed.set_thumbnail(url="https://www.logaster.com/blog/wp-content/uploads/2020/06/image14-3.png")
            strs = (f"{emoji} {name}" for name, emoji in sorted(status_list, key=lambda x: x[0]))
            embed.add_field(name="Users", value='\n'.join(strs), inline=False)
            embed.set_footer(text="Bot by Lex<3")
            await self.channel.send(embed=embed)

    async def send_disconnect(self):
        embed = discord.Embed(title=self.addr, color=0xA71056)
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/attachments/754599825240096828/754599903090442271/kisspng-computer-icons-vector-graphics-image-icon-design-i-flat-delete-icon-bing-images-5b7b43c032d3.png")
        embed.add_field(name="Server is offline", value='\u200b')
        embed.set_footer(text="Bot by Lex<3")
        await self.channel.send(embed=embed)

    async def send_reconnect(self):
        embed = discord.Embed(title=self.addr, color=0x009A44)
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/attachments/754599825240096828/754602941507305513/pngwing.com.png")
        embed.add_field(name="Server is online", value='\u200b')
        embed.set_footer(text="Bot by Lex<3")
        await self.channel.send(embed=embed)

    # endregion

    async def periodic_update(self):
        """
        Periodically executes the state machine and
        performs the correct actions for that state
        """
        while True:
            await self.execute()
            await asyncio.sleep(30)

    # region StateMachine
    async def execute(self):
        transitions = {
            MinecraftServer.States.CONNECTED: self._handle_connected,
            MinecraftServer.States.FAILING: self._handle_failing,
            MinecraftServer.States.DISCONNECTING: self._handle_disconnecting,
            MinecraftServer.States.DISCONNECTED: self._handle_disconnected
        }
        await transitions[self.state]()

    async def transition(self, state):
        self.state = state
        await self.execute()

    async def _handle_connected(self):
        self.failures = 0
        # noinspection PyBroadException
        try:
            player_states = self.update_players()
        except:
            await self.transition(MinecraftServer.States.FAILING)
        else:
            await self.send_status(*player_states)

    async def _handle_failing(self):
        # noinspection PyBroadException
        try:
            self.ping()
        except:
            self.failures += 1
            if self.failures == 2:
                await self.transition(MinecraftServer.States.DISCONNECTING)
        else:
            await self.transition(MinecraftServer.States.CONNECTED)

    async def _handle_disconnecting(self):
        self.players = set()
        await self.send_disconnect()
        await self.transition(MinecraftServer.States.DISCONNECTED)

    async def _handle_disconnected(self):
        with suppress(Exception):
            self.ping()
            await self.send_reconnect()
            await self.transition(MinecraftServer.States.CONNECTED)

    # endregion


def get_server(guild: int) -> Optional[MinecraftServer]:
    return servers.get(guild)
