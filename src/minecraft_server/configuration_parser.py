import argparse
import io
from contextlib import redirect_stdout

__parser = argparse.ArgumentParser()
__required = __parser.add_argument_group(title="Required")
__required.add_argument('--addr', help="Address (ip or hostname) of minecraft server")
__required.add_argument('--port', type=int, help="Port that server is listening, often 25575")


def parse(args):
    res = __parser.parse_known_args(args)
    res = res[0]  # parse_known_args returns tuple, we only want known args so index 0
    for k, v in vars(res).items():
        if v is None:
            raise ValueError("missing: {}".format(k))
    return res


def help_string(fixup):
    f = io.StringIO()
    with redirect_stdout(f):
        __parser.print_help()
    help_str = fixup(f.getvalue())
    return help_str
