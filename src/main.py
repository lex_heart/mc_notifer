import argparse

import global_state
import mcbot


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='mcbot.cfg', help='Location of configuration file')
    args = parser.parse_args()
    global_state.setup_global_state(args.config)
    mcbot.launch(global_state.configuration['bot']['token'])


if __name__ == '__main__':
    main()
