from configparser import ConfigParser
from datetime import datetime
from typing import Optional

from discord.ext import commands

configuration = ConfigParser()
bot = None  # type: Optional[commands.Bot]
start = datetime.utcnow()


def get_global_administrator():
    try:
        admin_id = int(configuration.get('administration', 'global_admin', fallback=0))
    except ValueError:
        # todo log error
        return 0
    else:
        return admin_id


def setup_global_state(file):
    configuration.read(file)
